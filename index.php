<?php
//建立資料庫連線參數
/*define("HOST", "sql303.byethost7.com");
define("ACCOUNT", "b7_26048860");
define("PASSWORD", "acg357653");
define("DATA", "b7_26048860_ishop");

//時區設定
date_default_timezone_set("Asia/Taipei");


//顯示錯誤
ini_set('display_errors', 'on');
ini_set('session.cookie_lifetime', 0);
ini_set('session.gc_maxlifetime', 1);
error_reporting(E_ALL ^ E_NOTICE);
session_start();

$link = mysqli_connect(HOST, ACCOUNT, PASSWORD, DATA);


if ($link) {
  echo '連線成功';
} else {
  echo '連線失敗';
}*/

?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
  <meta name="generator" content="Jekyll v4.0.1">
  <title>個人測試</title>

  <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/checkout/">

  <!-- Bootstrap core CSS -->
  <link href="../assets/dist/css/bootstrap.css" rel="stylesheet">

  <style>
    @charset "UTF-8";
    @import url(https://fonts.googleapis.com/earlyaccess/cwtexyen.css);
    @import url(https://fonts.googleapis.com/earlyaccess/notosanstc.css);

    .bd-placeholder-img {
      font-size: 1.125rem;
      text-anchor: middle;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    @media (min-width: 768px) {
      .bd-placeholder-img-lg {
        font-size: 3.5rem;
      }
    }
  </style>
  <!-- Custom styles for this template -->
  <link href="form-validation.css" rel="stylesheet">
</head>

<body class="bg-light">
  <div class="container">
    <div class="py-5 text-center">
      <img class="d-block mx-auto mb-4" src="../assets/brand/bootstrap-solid.svg" alt="" width="26" height="26">
      <h2>檔案上傳列表</h2>
      <p class="lead">檔案編號自動產生，您將會在檔案上傳後得到檢視網址。</p>
    </div>
    <div class="row">
      <div class="col-md-12 order-md-1">
        <h4 class="mb-3">輸入個人檔案資訊</h4>
        <form class="needs-validation" enctype="multipart/form-data" novalidate>
          <div class="row">
            <div class="col-md-12 mb-3">
              <label for="token">Token</label>
              <input type="text" class="form-control" id="token" placeholder="" value="" required>
              <div class="invalid-feedback">
                帳戶驗證碼必須輸入.
              </div>
            </div>
          </div>
          <div class="mb-3">
            <label for="email">Email <span class="text-muted">(Required)</span></label>
            <input type="email" class="form-control" id="email" placeholder="you@example.com" required>
            <div class="invalid-feedback">
              Please enter a valid email address for file updates.
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 mb-3">
              <label title="請選擇檔案類型，可選擇壓縮檔(Zip、RAR)、視頻及圖片(video、picture)以及一般文件(Pdf)。" for="type">檔案類型</label>
              <select class="custom-select d-block w-100" id="type" required>
                <option value="">請選擇檔案類型...</option>
                <option>Zip</option>
                <option>RAR</option>
                <option>video</option>
                <option>Picture</option>
                <option>File</option>
              </select>
              <div class="invalid-feedback">
                Please select a valid tyoe.
              </div>
            </div>
            <div class="col-md-6 mb-3">
              <label for="state">開放設定</label>
              <select class="custom-select d-block w-100" id="state" required>
                <option value="">Choose...</option>
                <option>Public</option>
                <option>Private</option>
              </select>
              <div class="invalid-feedback">
                Please provide a valid state.
              </div>
            </div>
          </div>
          <div class="col-md-12 mb-3">
            <input type="button" class="btn btn-info" value="檔案上傳" onclick="upload()" for="file"><label></label>
            <input type="file" id="file" name="file" hidden class="form-control" required />
            <label class="form-label" id="readme" name="readme">未選擇任何檔案</label>
          </div>
          <hr class="mb-4">
          <button class="btn btn-primary btn-lg btn-block" disabled type="submit">Continue to upload</button>
        </form>
      </div>
    </div>

    <footer class="my-5 pt-5 text-muted text-center text-small">
      <p class="mb-1">&copy; 2020 Xin-ning,chang</p>
    </footer>
  </div>
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script>
    window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')
    function upload(){
      $('#file').click();
    }
    $('#file').change(function(e) {
        var file = e.target.files;
        if(file.length>0)
        {
            // 獲取檔名 並顯示檔名
            var fileName = file[0].name;
            var size = file[0].size;
            $("#readme").html("<p>"+fileName+","+size+" byte.</p>");
        }else
        {
            //清空檔名
            $("#readme").html("<p>未選擇任何檔案</p>");
        }
    });

  </script>
  <script src="../assets/dist/js/bootstrap.bundle.js"></script>
  <script src="form-validation.js"></script>
</body>

</html>